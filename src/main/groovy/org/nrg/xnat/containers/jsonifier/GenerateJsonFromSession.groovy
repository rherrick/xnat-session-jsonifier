package org.nrg.xnat.containers.jsonifier

import groovy.io.FileType
import groovy.json.JsonOutput
import groovy.json.StreamingJsonBuilder
import groovy.util.logging.Slf4j
import org.dcm4che3.data.Attributes
import org.dcm4che3.data.Tag
import org.dcm4che3.io.DicomInputStream

import java.util.concurrent.Callable

@Slf4j
class GenerateJsonFromSession implements Callable<File> {
    public static int MAX_TAG = [Tag.Columns, Tag.InstanceNumber, Tag.Rows, Tag.SOPInstanceUID, Tag.SeriesDescription, Tag.SeriesInstanceUID, Tag.SeriesNumber].max() + 1

    GenerateJsonFromSession(String session, String patientName, String studyInstanceUid, String address, File folder) {
        this.session = session
        this.patientName = patientName
        this.studyInstanceUid = studyInstanceUid
        this.address = address

        final scansFolder = new File(folder, "SCANS")
        if (!scansFolder.exists()) {
            throw new RuntimeException("The SCANS folder doesn't exist under the session folder for the session ${session}.")
        }

        scansFolder.eachFile(FileType.DIRECTORIES) { File scanFolder ->
            final dicomFolder = new File(scanFolder, "DICOM")
            if (!dicomFolder.exists()) {
                log.info "The scan folder ${scanFolder} doesn't contain a DICOM folder, continuing."
            } else {
                dicomFolders << dicomFolder
            }
        }

        log.info("Found the following ${dicomFolders.size()} DICOM folders: ${dicomFolders}")
    }

    @Override
    File call() {
        def temp = new File("${session}.json.temp")
        temp.deleteOnExit()
        temp.withWriter { writer ->
            new StreamingJsonBuilder(writer) transactionId: "XNAT_DATA",
                    studies: [studyInstanceUid: "${studyInstanceUid}",
                              patientName     : patientName,
                              seriesList      : getSeries()]

        }
        def file = new File("${session}.json")
        file.withWriter { writer ->
            writer.write JsonOutput.prettyPrint(temp.text)
        }
        file
    }

    Set<SeriesFiles> getSeries() {
        final SortedSet<SeriesFiles> seriesFiles = new TreeSet<SeriesFiles>(new Comparator() {
            @Override
            int compare(final Object o1, final Object o2) {
                Integer i1 = o1.seriesNumber
                Integer i2 = o2.seriesNumber
                if (!i1 && !i2) {
                    0
                } else if (!i1) {
                    -1
                } else if (!i2) {
                    1
                } else {
                    Integer.compare(i1, i2)
                }
            }
        })
        seriesFiles.addAll(dicomFolders.collect { File dicomFolder ->
            new SeriesFiles(dicomFolder)
        })
        seriesFiles.removeAll { files ->
            !files.seriesDescription?.trim()
        }
        seriesFiles
    }

    class SeriesFiles {
        private static final FINDER = new FileNameByRegexFinder()

        SeriesFiles(final File dicomFolder) {
            def fileNames = FINDER.getFileNames(dicomFolder.absolutePath, ".*\\.dcm")

            // Check to make sure we found a DICOM file at all.
            if (fileNames.isEmpty()) {
                log.warn("No DICOM files found in the folder ${dicomFolder}")
                seriesInstanceUid = seriesDescription = seriesNumber = null
            } else {
                final Attributes topLevelAttributes = new DicomInputStream(new FileInputStream(fileNames.head())).withStream { dicomInputStream ->
                    dicomInputStream.readDataset(-1, MAX_TAG)
                }

                seriesInstanceUid = topLevelAttributes.getString Tag.SeriesInstanceUID
                seriesDescription = topLevelAttributes.getString Tag.SeriesDescription
                seriesNumber = topLevelAttributes.getInt Tag.SeriesNumber, 0

                dicomFolder.eachFileMatch(FileType.FILES, ~/.*\.dcm/) { dicomFile ->
                    dicomFile.withInputStream { input ->
                        new DicomInputStream(input).withStream { dicomInputStream ->
                            final Attributes attributes = dicomInputStream.readDataset(-1, MAX_TAG)

                            def instanceNumber = attributes.getInt Tag.InstanceNumber, 0
                            def sopInstanceUid = attributes.getString Tag.SOPInstanceUID
                            def columns = attributes.getInt Tag.Columns, 0
                            def rows = attributes.getInt Tag.Rows, 0

                            instances << [
                                    sopInstanceUid: sopInstanceUid,
                                    instanceNumber: instanceNumber,
                                    columns       : columns,
                                    rows          : rows,
                                    url           : "${address}/data/experiments/${session}/scans/${instanceNumber}/resources/DICOM/files/${dicomFile.name}"
                            ]
                        }
                    }
                }

                log.info("Finished processing series ${seriesNumber} \"${seriesDescription}\" containing ${instances.size()} files in the folder ${dicomFolder}")
            }
        }

        final seriesInstanceUid
        final seriesDescription
        final seriesNumber
        final SortedSet instances = new TreeSet(new Comparator() {
            @Override
            int compare(final Object o1, final Object o2) {
                int i1 = o1.instanceNumber
                int i2 = o2.instanceNumber
                return Integer.compare(i1, i2)
            }
        })
    }

    final session
    final patientName
    final studyInstanceUid
    final address
    final dicomFolders = []
}
