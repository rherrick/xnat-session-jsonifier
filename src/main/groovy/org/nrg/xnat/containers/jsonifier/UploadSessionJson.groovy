package org.nrg.xnat.containers.jsonifier

import groovy.util.logging.Slf4j
import groovyx.net.http.HttpResponseException
import groovyx.net.http.Method
import groovyx.net.http.RESTClient
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.MultipartEntityBuilder

import java.util.concurrent.Callable

@Slf4j
class UploadSessionJson implements Callable<Boolean> {
    UploadSessionJson(String address, String username, String password, String session, Boolean overwrite, File json) {
        client = new RESTClient("${address}/data/experiments/${session}/resources/metadata/")
        client.auth.basic username, password
        this.overwrite = overwrite
        this.json = json
    }

    @Override
    Boolean call() {
        // First make sure the resource folder's there for the JSON to land.
        try {
            client.put(path: "")
        } catch (HttpResponseException exception) {
            if (exception.response.status == 409) {
                log.debug "Got a 409 for the metadata PUT, this is no big deal, just means the folder already exists."
            } else {
                log.warn "Got the status code ${exception.response.status} for the metadata PUT, this might be bad but will continue. Stated cause is: ${exception.response.statusLine}"
            }
        }

        def post = client.request(Method.POST) { request ->
            requestContentType = ContentType.MULTIPART_FORM_DATA.toString()
            uri.path = "files"
            uri.query = [file_upload: true, content: "FRAMES", format: "json", overwrite: overwrite]

            request.setEntity MultipartEntityBuilder.create().addBinaryBody("local_file", json, ContentType.APPLICATION_JSON, json.name).build()

            response.success = { response ->
                println "Succeeded with POST response status: ${response.statusLine}"
            }
            response.failure = { response ->
                println "Failed with POST response statusline: ${response.statusLine}"
            }
        }
        true
    }

    final RESTClient client
    final boolean overwrite
    final File json
}
