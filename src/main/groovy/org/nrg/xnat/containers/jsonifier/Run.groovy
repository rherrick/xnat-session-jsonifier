package org.nrg.xnat.containers.jsonifier

def test = "--session XNAT_E00002 --patientName XNAT_01_01 --studyInstanceUid 1.2.840.113654.2.45.2.108105 --address http://xnatdev.xnat.org --username admin --password admin --overwrite --folder /Users/rherrick/Development/XNAT/Containers/xnat-session-jsonifier/data/XNAT_01_01_MR1".split()

def cli = new CliBuilder(usage: 'groovy GenerateJsonFromSession.groovy ')
cli.with {
    h longOpt: 'help', 'Show usage information'
    s longOpt: 'session', args: 1, required: true, 'The ID of the session being processed.'
    n longOpt: 'patientName', args: 1, required: true, 'The patient name for the session being processed.'
    i longOpt: 'studyInstanceUid', args: 1, required: true, 'The study instance UID for the session being processed.'
    a longOpt: 'address', args: 1, required: true, 'Specifies the server address for your XNAT installation.'
    u longOpt: 'username', args: 1, required: true, 'The username for the XNAT user'
    p longOpt: 'password', args: 1, required: true, 'The password for the XNAT user'
    f longOpt: 'folder', args: 1, required: true, 'The folder containing the DICOM data to parse.'
    o longOpt: 'overwrite', 'Indicates whether the generated JSON should overwrite any existing session JSON resource.'
}

def options = cli.parse(args == null || args.length == 0 ? test : args)

if (options.h) {
    cli.usage()
    return
}

String session = options.s
String patientName = options.s
String studyInstanceUid = options.i
String address = options.a
String username = options.u
String password = options.p
File folder = new File(options.f as String)
boolean overwrite = options.o as Boolean

GenerateJsonFromSession generator = new GenerateJsonFromSession(session, patientName, studyInstanceUid, address, folder)
File json = generator.call()

if (!json.exists()) {
    println "No JSON file found."
    return
}

println "Got the JSON file ${json.name} with ${json.size()} bytes, preparing to upload to ${address} for session ${session}"
UploadSessionJson upload = new UploadSessionJson(address, username, password, session, overwrite, json)
boolean success = upload.call()
if (success) {
    println "Hooray, I uploaded the stuff!"
} else {
    println "Boo, I failed to upload the stuff!"
}

