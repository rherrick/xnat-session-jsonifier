# XNAT Session Jsonifier #

Creates JSON from a specified session and uploads the JSON back as a session resource in the folder **metadata** with the format **json** and content **FRAMES**. This JSON is intended for use with the Cornerstone viewer.

This also creates a Docker container image with the Jsonifier as an XNAT container image.

